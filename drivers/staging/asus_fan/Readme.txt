ASUS (Zenbook) fan(s) control kernel module. 

From: Markus Meissner
https://github.com/daringer/asus-fan

The following Zenbooks are supported:

Single Fan		Two Fans (NVIDIA)
UX21E			UX32VD
UX31E			UX42VS
UX21A			UX52VS
UX31A			U500VZ
UX32A			NX500
UX301LA	
UX302LA
